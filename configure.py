from voluptuous import Schema, Invalid, Optional, Url, In, Unique, Length, All, Any, Required
from ruamel.yaml import load, RoundTripLoader, compose
from io import StringIO

PinDef = Schema({
    'name': str,
    Optional('type', default="bidirectional"): In(["bidirectional","power_in","power_out","passive"]),
    Optional('position', default="left"): In(["left",'right',"top","bottom"]),
    Required('number'): int
})

def unique_pin_number(pins):
    seen = set()
    dupes = [x["number"] for x in pins if x["number"] in seen or seen.add(x["number"])]
    if len(dupes) >0:
        raise Invalid('contains duplicate pin number: {0}'.format(dupes))
    
    return pins

Part = Schema({
    'name': str,
    'prefix': str,
    Required('pins'): All([PinDef], unique_pin_number, Length(min=1)),
    Optional('description'): str,
    Optional('datasheet'): Url,
    Optional('pad_width', default=1.7): Any(None, int, float),
    Optional('drill_hole', default=1.0): Any(None, int, float),
    Optional('font_height', default=1): Any(None, int, float),
    Optional('pitch', default=2.54): Any(None, int, float),
    Optional('width'): Any(None, int, float),
    Optional('length'): Any(None, int, float),
    Optional('height'): Any(None, int, float),
    
})

Library = Schema({
    'name': str,
    'parts': All([Part], Length(min=1))
})

data = """
name: my project
parts: 
  - name: bme680
    prefix: U
    pins:
        - name: A0
          number: 1
        - name: A1
          number: 1
        - name: A2
          number: 2
"""

class Configuration():
    def __init__(self, stream):
        self.data = None
        self.validate(stream)

    def validate(self,stream):
        try:
            self._yaml = load(stream, Loader=RoundTripLoader)
            self.data = Library(self._yaml)
        except Invalid as e:
            print(f"err path {e.args}")
            print(self._yaml)
            node = (self._yaml)
            if "path" in node:
                for key in e.path:
                    if (key in node and hasattr(node[key], '_yaml_line_col')):
                        node = node[key]
                    else:
                        break
                path = '/'.join(e.path)
            else:
                path = ""
            raise Invalid(f"Error: validation failed on line {node._yaml_line_col.line}:{node._yaml_line_col.col} (/{path}): {e.error_message}")
        else:
            return self.data
        
cfg = Configuration(StringIO(data))
print(cfg.data)