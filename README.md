# HobbyFarm

A set of tools to generate KiCad projects for not so serious users, in fact I am pretty sure there are better ways to do all of these things.  So in full faith that someone will feel compelled to correct someone else who is wrong on the internet I release these in hopes said better ways 😁.

I made these because I couldn't find a tool to assist me.  Since I wanted something that was self-hosted, open source, and had an active community KiCad fit most of the bill.  Unfortunately it was made with real hardware engineers in mind so it doesn't really focus on things that use breakout boards on breadboards.  So these have been born.

## To setup

```bash
conda env update -f environment.yml
```

To use the tools:

```bash
conda activate hobbyfarm
```

## Tools

### `hobbyfarm.py`

This tool will generate a symbol, footprint, and 3d model for custom parts.  It's really made for breakout boards since pretty much everything else can be better served by KiCad proper. This assumes all the pins are on a header, and the boards width is the width of them.

YAML format:

Toplevel:

* `library`: The name that will be used for the symbol and footprint libraries.
* `parts`: A list of parts that will be in this library.

Parts:

* `name`:  The name that will be displayed on the symbol and footprint.
* `description`: Just some human text to remind you 6 months from know what this is.
* `datasheet`: I usually just put the URL for the part on Seeed/Adafuit/M5/Lilygo/etc.
* `pitch`: 99.9% of the time its 2.54.
* `package_height`: This is used on the 3d model to give the part thickness.  I use it to eyeball placement in a project case so its just a box.
* `header_height`: This is the header added to the board if any.  Some times I use a larger header to get further off the board.
* `board_length`: How long the board is for the footprint.  The width is automaticly calculated by the number of pins and the pitch.
* `pad_width`: Not sure why I need this.
* `drill_hole`: This is really just for being able to line it up on the breadboard.
* `font_height`: This makes the text larger or smaller on the footprint.
* `prefix`: This is the prefix for the reference on the board.
* `pinDefs`:  This is a list of the pin definitions for the breakout board.

Pin Definitions:

* `name`: The name of the pin on the symbol and the footprint.
* `electricalType`:  One of `power_in`,`passive`,`bidirectional`,.
* `position`: The side that the pin is drawn on the schematic.  Can be one of `left`,`right`,`top`,`bottom`.
* `number`: The order of the pins on the header.

### `perfboard_gen.py`

This will generate a starter project that comes with a generated [Adafuit Perma-Proto Breadboards](https://www.adafruit.com/category/466).   This helps with planning out placement of the above boards.  For more details on how to use this see [this tutorial](http://).  Right now only [Full](https://www.adafruit.com/product/1606), [Half](https://www.adafruit.com/product/1609), and [Quarter](https://www.adafruit.com/product/1608) boards.  The [Flex](https://www.adafruit.com/product/1518) should also work. If there are suggestions for other boards please submit a ticket this the specifications.
