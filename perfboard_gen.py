from cmath import sqrt
import uuid
from kiutils.board import Board
from kiutils.items.brditems import Via, Segment, LayerToken
from kiutils.items.common import Position, Effects, Font
from kiutils.items.dimensions import Dimension
from kiutils.items.gritems import GrRect, GrText
from kiutils.schematic import Schematic, TitleBlock
from pathlib import Path
from re import sub

output_dir = Path("Basement LED Driver")
project_name = "LED Controller for basement LED"
filename = sub(r"\W+", "_", project_name).rstrip("_").lower()
perfboard_size = "half"
grid = 0.254


def align_to_grid(value):
    return value // grid * grid


x_start = 4
y_start = 4
board_width = 0
board_length = 0
holes = 0
if perfboard_size == "full":
    holes = 60
    board_width = 51
    board_length = 158
if perfboard_size == "half":
    holes = 30
    board_width = 51
    board_length = 81
if perfboard_size == "quarter":
    holes = 15
    board_width = 51
    board_length = 44

origin = (align_to_grid(25), align_to_grid(25))
border = (
    origin[0],
    origin[1],
    origin[0] + align_to_grid(board_width),
    origin[1] + align_to_grid(board_length),
)
base_x = border[0] + align_to_grid(x_start)
base_y = border[1] + align_to_grid(y_start)

vias = []
traces = []
letters = [
    GrRect(
        start=Position(border[0], border[1], 0),
        end=Position(border[2], border[3], 0),
        layer="Edge.Cuts",
        tstamp=uuid.uuid4(),
        locked=True
    )
]
top_row = ["-", "+", "", "J", "I", "H", "G",
           "F", "", "", "E",
           "D", "C", "B", "A", "", "-", "+"]

for x in range(0, 18):
    if x not in [2, 8, 9, 15]:
        letters.append(
            GrText(
                top_row[x],
                position=Position(base_x + (x * 2.54),
                                  border[1] + 2, 90),
                layer="Dwgs.User",
                tstamp=uuid.uuid4(),
                locked=True,
            )
        )
        letters.append(
            GrText(
                top_row[x],
                position=Position(base_x + (x * 2.54),
                                  border[3] - 2, 90),
                layer="Dwgs.User",
                tstamp=uuid.uuid4(),
                locked=True,
            )
        )

for x in range(0, 18):
    for y in range(0, holes):
        if x in [2, 15]:
            letters.append(
                GrText(
                    str(30 - y),
                    position=Position(base_x + (x * 2.54),
                                      base_y + (y * 2.54), 90),
                    layer="Dwgs.User",
                    tstamp=uuid.uuid4(),
                    locked=True,
                )
            )
        elif x not in [2, 8, 9, 15]:
            traces.append(
                Via(
                    position=Position(base_x + (x * 2.54),
                                      base_y + (y * 2.54), 0),
                    layers=["F.Cu", "B.Cu"],
                    size=1.0,
                    drill=0.6,
                    tstamp=uuid.uuid4(),
                )
            )
            # vertical strips
            if y > 0 and x in [0, 1, 16, 17]:
                traces.append(
                    Segment(
                        start=Position(base_x + (x * 2.54),
                                       base_y + (y * 2.54), 0),
                        end=Position(base_x + (x * 2.54),
                                     base_y + ((y - 1) * 2.54), 0),
                        layer="B.Cu",
                        width=0,
                        tstamp=uuid.uuid4(),
                    )
                )
            # horizontal strips
            if x in [4, 5, 6, 7, 11, 12, 13, 14]:
                traces.append(
                    Segment(
                        start=Position(base_x + (x * 2.54),
                                       base_y + (y * 2.54), 0),
                        end=Position(base_x + ((x - 1) * 2.54),
                                     base_y + (y * 2.54), 0),
                        layer="B.Cu",
                        width=0,
                        tstamp=uuid.uuid4(),
                    )
                )


traces.extend(vias)

board = Board.create_new()
board.generator = "hobbyfarm"
board.titleBlock = TitleBlock(project_name)
board.traceItems = traces
board.setup.gridOrigin = Position(border[0], border[1], 0)
board.graphicItems = letters


output_dir.mkdir(parents=True, exist_ok=True)

outfile = output_dir.joinpath(f"{filename}.kicad_pcb")
board.to_file(outfile, "utf-8")

schematic = Schematic.create_new()
schematic.titleBlock = TitleBlock(project_name)
schematic.generator = "hobbyfarm"
outfile = output_dir.joinpath(f"{filename}.kicad_sch")
schematic.to_file(outfile, "utf-8")

project = f'["meta": {{"filename": "{filename}.kicad_pro","version": 1}}]'

outfile = output_dir.joinpath(f"{filename}.kicad_pro")

with open(outfile, "w", encoding="utf-8") as f:
    f.write(project)
