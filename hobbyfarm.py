from kiutils.footprint import Footprint, Attributes, Pad, DrillDefinition, Model
from kiutils.items.common import Position, Effects, Font, Stroke, Property, ColorRGBA, Justify
from kiutils.items.fpitems import FpLine, FpText, FpRect
from kiutils.symbol import Symbol, SymbolPin, SymbolLib
from kiutils.items.syitems import SyRect, SyText
import cadquery as cq
from typing import List
from pathlib import Path
import webcolors
from ruamel.yaml import YAML, RoundTripLoader
import re
# Input: Pin Count, H, Font Height, Prefix
# Pin Width (2.75,2.0)
# Width ( (PinWidth + 2.45) * Pin Count + 2.45 )
# X1,Y1 (-W/2, -H/2)
# X2,Y2 (W/2, H/2)
# Text Pos (-W/2,H-(Font Height * 2) )


"""
Given board height = H
Given Pin length = pinH
Given Header thickness = headerH

Create workplane at offset H + headerH
Create rect for F.SilkS and extrude down H

Create rect for "F.Fab" and extrude down headerH

Create workplane at <Z
For each pad create a circle the size of drill hole, extrude down pinH

export step to f"{output_dir}/3dmodels/{library}.3dshapes/{name}.step
export vrml to f"{output_dir}/3dmodels/{library}.3dshapes/{name}.wrl
"""


def clean_name(name):
    return re.sub(r"\W+", "_", name).rstrip("_").lower()


def resolve_color(hex: str, alpha=1):
    col = webcolors.hex_to_rgb(hex)
    return cq.Color(col.red/255, col.green/255, col.blue/255, alpha)


def align_to_grid(value):
    grid = 0.254
    return value // grid * grid


def aligned_Position(X, Y, angle=None, unlocked=False):
    return Position(align_to_grid(X), align_to_grid(Y), angle, unlocked)


def key_pad(idx):
    if idx == 1:
        return "rect"
    else:
        return "circle"


def make_pins(pitch, pad_width, drill_hole, pin_count, x1, y1):
    pin_start = x1 - (pitch/2)
    pads = [
        Pad(f"{x}",
            type="thru_hole",
            shape=key_pad(x),
            drill=DrillDefinition(diameter=drill_hole),
            position=aligned_Position(
                X=pin_start + (x * pitch),
                Y=y1+(pitch/2)
            ),
            size=Position(pad_width[0], pad_width[1]),
            layers=["*.Cu", "*.Mask"],
            ) for x in range(1, pin_count+1)
    ]

    return pads


def export_shapes(output_dir: Path, name, outline: FpRect, header: FpRect, pins: List[Pad], package_height: float, header_height: float, pin_length: float, drill_hole: float):
    header_length = header.start.X - header.end.X
    header_width = header.start.Y - header.end.Y
    x1 = outline.start.X
    x2 = outline.end.X
    y1 = outline.start.Y
    y2 = outline.end.Y
    package_length = y2 - y1
    package_width = x2 - x1

    points = [(pin.position.X, -pin.position.Y) for pin in pins]

    component = cq.Assembly()
    """
        result: cq.Workplane = (
            cq.Workplane()
            .box(x2-x1, y2-y1, package_height+header_height, centered=(True, True, False))
            .faces("<Z").edges(">Y").center(0, header_width/2)
            .rect((x2-x1), header_width-(y2-y1)).extrude(header_height, combine="cut")
            .faces("<Z").workplane(centerOption="CenterOfMass")
            .workplane().pushPoints(points)
            .circle(drill_hole / 2.0)
            .extrude(pin_length)
        )
    """
    case = cq.Workplane("XY").workplane(offset=0.0, centerOption="CenterOfMass").moveTo(0.0, 0.0).rect(
        package_width, package_length).extrude(package_height).translate((0.0, 0.0, header_height))
    head = cq.Workplane("XY").workplane(offset=0.0, centerOption="CenterOfMass").moveTo(
        0, 0).rect(header_length, header_width).extrude(header_height).translate((0, y2+(header_width/2), 0))
    pins = cq.Workplane("XY").workplane(offset=-pin_length, centerOption="CenterOfMass").moveTo(
        0, 0).pushPoints(points).circle(drill_hole / 4.0).extrude(pin_length)
    component.add(case, color=resolve_color("#094D1C", 0.5))
    component.add(head, color=resolve_color("#000000"))
    component.add(pins, color=resolve_color("#E1C16E"))
    component.name = f"{name}"

    shapedir = output_dir.joinpath(f"3dmodels/{clean_name(library)}.3dshapes")
    shapedir.mkdir(parents=True, exist_ok=True)
    outfile = shapedir.joinpath(f"{clean_name(name)}.stp")
    component.save(str(outfile), cq.exporters.ExportTypes.STEP,
                   mode=cq.exporters.assembly.ExportModes.FUSED, write_pcurves=False)
    return [
        # ${HOBBYFARM_DIR}/KiCad/7.0/3dmodels/Custom_Parts.3dshapes/testmod.stp
        Model(f"${{HOBBYFARM_DIR}}/{shapedir}/{clean_name(name)}.stp")
    ]


def build_footprint(output_dir: Path, name: str, description: str, library: str, font_height: float, prefix: str, textpos: float, pads: List[Pad], outline: FpRect, header: FpRect, models: List[Model]):
    footprint = Footprint(
        library,
        entryName=name,
        version="1.0",
        generator="HobbyFarm",
        description=description,
        tags="hobbyfarm",
        attributes=Attributes("through_hole"),
        pads=pads,
        models=models,
        graphicItems=[
            outline,
            header,
            FpText(
                type="reference",
                text=f"{prefix}",
                position=Position(textpos[0], textpos[1]),
                layer="F.SilkS",
                effects=Effects(font=Font(height=font_height,
                                          width=font_height, thickness=0.15))
            ),
            FpText(
                type="value",
                text=name,
                position=Position(textpos[0], textpos[1] + font_height + 2),
                layer="F.SilkS",
                effects=Effects(font=Font(height=font_height,
                                          width=font_height, thickness=0.15))
            ),
            FpText(
                type="user",
                text="${REFERENCE}",
                position=Position(
                    textpos[0], textpos[1] + ((font_height + 2)*2)),
                layer="F.Fab",
                effects=Effects(font=Font(height=font_height,
                                          width=font_height, thickness=0.15))
            ),
        ]
    )

    outfile = output_dir.joinpath(f"{clean_name(library)}.pretty")
    outfile.mkdir(parents=True, exist_ok=True)
    outfile = outfile.joinpath(f"{clean_name(name)}.kicad_mod")
    footprint.to_file(outfile, "utf-8")

    return outfile


def create_base(props, x: float, y: float, angle: float, pinLength: float):
    return ({
        "name": props["name"],
        "electricalType": props["electricalType"],
        "length": pinLength,
        "number": str(props['number']),
        "position": aligned_Position(x, y, angle),
    })


def build_symbol(name: str, datasheet: str, library: str, output_dir: Path, font_height: float, prefix: str, pinDefs) -> Symbol:
    leftPins = [x for x in pinDefs if x["position"] == "left"]
    rightPins = [x for x in pinDefs if x["position"] == "right"]
    topPins = [x for x in pinDefs if x["position"] == "top"]
    bottomPins = [x for x in pinDefs if x["position"] == "bottom"]

    symbol_width = max(len(topPins), len(bottomPins),
                       len(name)) * font_height*2
    symbol_height = max(len(leftPins), len(
        rightPins), len(name)) * font_height*2
    x1 = (-symbol_width/2)
    y1 = (-symbol_height/2)
    x2 = (symbol_width/2)
    y2 = (symbol_height/2)
    textpos = (0, 0-(font_height*2))
    pinLength = 2.54

    pins = []

    offset = (symbol_width/2)-(len(leftPins) * font_height)
    for index, x in enumerate(leftPins):
        pins.append(
            SymbolPin(
                nameEffects=Effects(font=Font(height=font_height,
                                              width=font_height, thickness=0.15)),
                numberEffects=Effects(
                    font=Font(height=font_height, width=font_height, thickness=0.15)),
                **create_base(x, x1 - pinLength, y1 + (index*font_height*2) + offset, 0, pinLength)
            )
        )

    offset = (symbol_width/2)-(len(rightPins) * font_height)
    for index, x in enumerate(rightPins):
        pins.append(
            SymbolPin(
                nameEffects=Effects(font=Font(height=font_height,
                                              width=font_height, thickness=0.15)),
                numberEffects=Effects(
                    font=Font(height=font_height, width=font_height, thickness=0.15)),
                **create_base(x, x2 + pinLength, y1 + (index*font_height*2)+offset, 180, pinLength)
            )
        )

    offset = (symbol_height/2)-(len(topPins) * font_height)
    for index, x in enumerate(topPins):
        pins.append(
            SymbolPin(
                nameEffects=Effects(font=Font(height=font_height,
                                              width=font_height, thickness=0.15)),
                numberEffects=Effects(
                    font=Font(height=font_height, width=font_height, thickness=0.15)),
                **create_base(x,  x1 + (index*font_height*2) + offset, y1 - pinLength, 90, pinLength)
            )
        )

    offset = (symbol_height/2)-(len(topPins) * font_height)
    for index, x in enumerate(bottomPins):
        pins.append(
            SymbolPin(
                nameEffects=Effects(font=Font(height=font_height,
                                              width=font_height, thickness=0.15)),
                numberEffects=Effects(
                    font=Font(height=font_height, width=font_height, thickness=0.15)),
                **create_base(x, x1 + (index*font_height*2) + offset, y2 + pinLength, 270, pinLength)
            )
        )

    return Symbol(
        properties=[
            Property("Reference", f"{prefix}", effects=Effects(justify=Justify(vertically="top")),
                     position=Position(0, font_height*2, 0)),
            Property("Footprint", f"{library}:{clean_name(name)}",
                     effects=Effects(hide=True)),
            Property("Datasheet", datasheet, effects=Effects(
                hide=True)),
            Property("Value", name, effects=Effects(
                justify=Justify(vertically="bottom"))),
        ],
        entryName=name,
        pins=pins,
        onBoard=True,
        inBom=True,
        graphicItems=[
            SyRect(start=aligned_Position(x1, y1, 0), end=aligned_Position(x2, y2),
                   stroke=Stroke(width=0.75, type="solid")),
        ],
    )


def make_part(pitch, name, description, datasheet, library, output_dir, pad_width, drill_hole, font_height, prefix, pinDefs, package_height=None, header_height=None, board_length=None):
    output_dir.mkdir(parents=True, exist_ok=True)

    pin_count = len(pinDefs)
    width = pin_count * pitch
    if board_length == None:
        board_length = width * 1.5
    if package_height == None:
        package_height = 1.7
    if header_height == None:
        header_height = 1.7
    x1 = -width/2
    y1 = -board_length/2
    x2 = width/2
    y2 = board_length/2
    textpos = (0, 0-(font_height*2))

    pads = make_pins(pitch, pad_width, drill_hole, pin_count, x1, y1)

    for pad in pads:
        print(f"Pad {pad.position}")

    outline, header = (FpRect(
        start=Position(x1, y1),
        end=Position(x2, y2),
        layer="F.SilkS",
        width=0.12
    ), FpRect(
        start=Position(
            x1 + (pitch/8),
            y1 + (pitch/8)
        ),
        end=Position(
            x2 - (pitch/8),
            y1 + (pitch/8) + (pitch/8) + pad_width[1]
        ),
        layer="F.Fab",
        width=0.1
    ))

    models = export_shapes(output_dir, name, outline, header,
                           pads, package_height, header_height, 4, drill_hole)

    footprint = build_footprint(output_dir, name, description, library,
                                font_height, prefix, textpos, pads, outline, header, models)

    return build_symbol(name, datasheet, library, output_dir,
                        font_height, prefix, pinDefs)


# Tunables
# library = "Custom_Parts"
output_dir = Path("KiCad/7.0/")

yaml = YAML(typ='rt')
input_file = yaml.load(Path("testparts.yml"))

library = input_file["library"]
parts = input_file["parts"]
symbols = [make_part(library=library, output_dir=output_dir, **x)
           for x in parts]


symbol_lib = SymbolLib(generator="HobbyFarm", symbols=symbols)
outfile = output_dir.joinpath(f"{clean_name(library)}.kicad_sym")
symbol_lib.to_file(outfile, "utf-8")
